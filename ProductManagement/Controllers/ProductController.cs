﻿using ProductManagement.Data;
using ProductManagement.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ProductManagement.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public async Task<ActionResult> Index()
        {
            ProductRepository productRepository = new ProductRepository();
            return View(await productRepository.GetAll());
        }

        public ActionResult AddProduct()
        {
            return View();
        }

        public async Task<ActionResult> EditProduct(int id)
        {
            ProductRepository productRepository = new ProductRepository();
            return View(await productRepository.Get(id));
        }

        [HttpPost]
        public async Task<ActionResult> Save(ProductViewModel model)
        {
            ProductRepository productRepository = new ProductRepository();
            await productRepository.Add(model);
            return RedirectToAction("Index","Product");
        }

        [HttpPost]
        public async Task<ActionResult> Update(ProductViewModel model)
        {
            ProductRepository productRepository = new ProductRepository();
            await productRepository.Update(model);
            return RedirectToAction("Index", "Product");
        }

        public async Task<ActionResult> Delete(int id)
        {
            ProductRepository productRepository = new ProductRepository();
            await productRepository.Delete(id);
            return RedirectToAction("Index", "Product");
        }
    }
}