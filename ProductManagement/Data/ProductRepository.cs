﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Threading.Tasks;
using ProductManagement.Models.ViewModels;
using System.Data.SqlClient;

namespace ProductManagement.Data
{
    public class ProductRepository
    {
        private readonly string _connectionString;

        public ProductRepository()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["connDB"].ConnectionString;
        }

        public async Task<ProductViewModel> Get(int id)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("get_product", connection))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@id", id));
                    var response = new ProductViewModel();
                    await connection.OpenAsync();

                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            response = ToProduct(reader);
                        }
                    }
                    return response;
                }
            }
        }

        public async Task Delete(int id)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("delete_product", connection))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@id", id));
                    await connection.OpenAsync();

                    await cmd.ExecuteNonQueryAsync();

                    return;
                }
            }
        }

        public async Task Add(ProductViewModel model)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("create_product", connection))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@name", model.Name));
                    cmd.Parameters.Add(new SqlParameter("@desc", model.Desc));
                    cmd.Parameters.Add(new SqlParameter("@price", model.Price));
                    await connection.OpenAsync();

                    await cmd.ExecuteNonQueryAsync();
                    
                    return;
                }
            }
        }

        public async Task Update(ProductViewModel model)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("edit_product", connection))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@id", model.Id));
                    cmd.Parameters.Add(new SqlParameter("@name", model.Name));
                    cmd.Parameters.Add(new SqlParameter("@desc", model.Desc));
                    cmd.Parameters.Add(new SqlParameter("@price", model.Price));
                    await connection.OpenAsync();

                    await cmd.ExecuteNonQueryAsync();

                    return;
                }
            }
        }

        public async Task<List<ProductViewModel>> GetAll()
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                using(SqlCommand cmd = new SqlCommand("get_products", connection))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    var response = new List<ProductViewModel>();
                    await connection.OpenAsync();

                    using(var reader = await cmd.ExecuteReaderAsync())
                    {
                        while(await reader.ReadAsync())
                        {
                            response.Add(ToProduct(reader));
                        }
                    }
                    return response;
                }
            }
        }

        private ProductViewModel ToProduct(SqlDataReader reader)
        {
            return new ProductViewModel()
            {
                Id = (int)reader["id"],
                Name = reader["product_name"].ToString(),
                Desc = reader["product_description"].ToString(),
                Price = (int)reader["product_price"]
            };
        }
    }
}